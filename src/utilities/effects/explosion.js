class Explosion {

    constructor(){
        this.movementSpeed = 1;
        this.totalObjects = 1000;
        this.objectSize = 0.1;
        this.sizeRandomness = 100;
        this.colors = [0xFF1900, 0xFFFF00];
        /////////////////////////////////
        this.dirs = [];
        this.parts = [];
    }

    init(x,y,z){
        var geometry = new THREE.Geometry();
        for (var i = 0; i < this.totalObjects; i ++) 
        { 
          var vertex = new THREE.Vector3();
          vertex.x = x;
          vertex.y = y;
          vertex.z = z;
        
          geometry.vertices.push( vertex );
          this.dirs.push({x:(Math.random() * this.movementSpeed)-(this.movementSpeed/2),y:(Math.random() * this.movementSpeed)-
            (this.movementSpeed/2),z:(Math.random() * this.movementSpeed)-(this.movementSpeed/2)});
        } 
        var material = new THREE.ParticleBasicMaterial( { size: this.objectSize,  
            color: this.colors[Math.round(Math.random() * this.colors.length)] });
        var particles = new THREE.ParticleSystem( geometry, material );

        this.object = particles;
        this.status = true;
        
        this.xDir = (Math.random() * this.movementSpeed)-(this.movementSpeed/2);
        this.yDir = (Math.random() * this.movementSpeed)-(this.movementSpeed/2);
        this.zDir = (Math.random() * this.movementSpeed)-(this.movementSpeed/2);

        scene.add( this.object  ); 

    }
    update(){
        if (this.status == true){
            var pCount = this.totalObjects;
            while(pCount--) {
            var particle =  this.object.geometry.vertices[pCount]
                particle.y += (this.dirs[pCount].y)/2;
                particle.x += this.dirs[pCount].x;
                particle.z += (this.dirs[pCount].z)*10;
            }
            this.object.geometry.verticesNeedUpdate = true;
        }
    }
    run(){

    }

}