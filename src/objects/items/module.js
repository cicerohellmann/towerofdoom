class Module {
    constructor(name_,integrity_,maintenanceTime_,complexity_, operateRequirements_){
        this.name = "bioreactor-H-5000";
        this.isMaintenanceTime = false;
        this.integrity = 10000;
        this.maintenanceTime = 10001.0;
        this.maintenanceTimeMax = 10001.0;
        this.maintenanceTimeExpected = 6000.0;
        this.deterioration = 20;
        this.integrityDeterioration = 3;
        this.complexity = 2;
        this.operationRequirement = [new OperationRequirement(new SkillType().REPAIRING, 5), 
        new OperationRequirement(new SkillType().STRENGHT, 5)];
    }

    update(delta){

        if(this.maintenanceTime>0){
            this.maintenanceTime -= this.deterioration;
        }
        if(this.maintenanceTime<=0){
            this.maintenanceTime = 0;
            this.integrity -= this.integrityDeterioration;
        }
        if(this.maintenanceTime > this.maintenanceTimeExpected ){
            this.isMaintenanceTime = false;
        }
        if(this.maintenanceTime < this.maintenanceTimeExpected && this.isMaintenanceTime == false){
            this.isMaintenanceTime = true;
        }
    }
}