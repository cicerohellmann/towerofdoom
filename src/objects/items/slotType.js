class SlotType {
	constructor(){
		this.MAINHAND = "MAINHAND";
		this.OFFHAND = "OFFHAND";
		this.HEAD = "HEAD";
		this.CHEST = "CHEST";
		this.LEGS = "LEGS";
		this.HANDS = "HANDS";
		this.FEET = "FEET";
		this.ARMS = "ARMS";
	}
}