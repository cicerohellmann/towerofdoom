class Wight {
    //species -> Human,reptile,tree,pig and etc
    constructor(age_,alive_,attributes_,equipment_,gender_,livingConditions_,name_,perks_,skills_,species_,home_,where_){
        this.initiativeCurrent;
        this.crewID = Math.floor(Math.random() * 9999999);
        this.age = age_;
        this.alive = alive_;
        this.disabled = false;
        this.attributes = attributes_;
        this.equipment = [];
        this.equipment = equipment_;
        this.gender = gender_;
        this.intelligent = this.attributes.isIntelligent();
        this.life = this.attributes.life(); 
        this.livingConditions = livingConditions_;
        this.initiative = 0;
        this.name = name_;
        this.perks = perks_;
        this.skills = skills_;
        this.species = species_;
        this.home = home_;
        this.where = where_;

        //EquipmentBonus//
        //Armor//
        this.totalArmor = 0;
        this.mainHand = 0;
        this.offHand = 0;
        this.getBonusFromEquipment();
        this.dc = this.armorBonus(); //armor check
        // console.log(this.mainHand)
        // console.log(this.hit(this.mainHand))
        // console.log(this.attack(this.mainHand))
        this.attack(this.mainHand)
        console.log("End of Chap: "+this.name)

    }
    getBonusFromEquipment(){
        this.getBonusFromArmor();
        this.getBonusFromWeapon();
    }

    getBonusFromArmor(){
        this.equipment.forEach(equipment => {
            if(equipment.itemType == new EquipmentType().ARMOR){
                this.totalArmor = this.totalArmor + equipment.dc;
            }
        }, this)
    }

    getBonusFromWeapon(){
        this.equipment.forEach(equipment => {
            if(equipment.itemType == new EquipmentType().WEAPON){
                equipment.skill = this.attributes.getRelevantAttribute(equipment.skillType).level;
                if(equipment.slotType == new SlotType().MAINHAND){
                    this.mainHand = equipment;
                }else if(equipment.slotType == new SlotType().OFFHAND){
                    this.offHand = equipment;
                }
            }
        }, this)
    }

    armorBonus(){
        return 10 + this.totalArmor;
    }

    hit(weapon){
        return (new Dice(20).rollDice()+weapon.skill);
    }

    attack(weapon){
        var totalDamage = 0;
        weapon.damage.forEach(damage => {
            totalDamage = totalDamage + damage.rollDice() + weapon.skill;
        }, this);
        return totalDamage;
    }

    defense(){
        return this.dc;
    }


    makingAlive(){
        this.alive = true;
    }

    makingDie(){
        this.alive = false;
    }
}