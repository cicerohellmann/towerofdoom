class Chap extends Wight {
    constructor(age_,alive_,attributes_,equipment_,gender_,livingConditions_,name_,perks_,skills_,species_,home_,where_){
        super(age_,alive_,attributes_,equipment_,gender_,livingConditions_,name_,perks_,skills_,species_,home_,where_);
        // this.profession = this.skills.skillHighest();
        this.skillType = new SkillType();
        this.skills = new Skills();
        this.didISayHi = false;
        this.salutation = "Hey! I'm " + this.name + " !";
        this.working = false;
        this.order = "";
        this.operationRequirement;
        this.thirst = 100;
        this.hunger = 100;
        this.money = 100;
        this.itemList;
    }

    biology(){
        this.thirst-=0.5;
        this.hunger-=0.5;
        if(this.hunger <= 50|| this.thirst <= 50){
            // this.makePurchase(intentionBuilder("THIRSTHY"));  
        }
    }

    makePurchase(type){
        this.where.establishments[0].products.forEach(function(product){
            if(type == product.itemType){
                this.itemList.push(product);
                this.money = (this.money - product.price);
                var index = this.where.establishments[0].products.indexOf(product);
                if (index > -1) {
                    this.where.establishments[0].products.splice(index, 1);
                }
            }
        }, this);
    }

    intentionBuilder(intention){
        switch(intention) {
          case "THIRSTHY":
                return new ItemType().DRINKING;
            break;
          case "HUNGRY":
                return new ItemType().FEEDING;
            break;
        }
    }

    watchOrders(workingOrders){
        if(this.working == false){
            workingOrders.forEach(function(order){
                
                var canITakeIt = false;
                order.objective.operationRequirement.forEach(function(attribute){
                    if(this.skills.getRelevantSkill(attribute.skill) !== "" || this.attributes.getRelevantAttribute(attribute.skill) !== "" ){
                        canITakeIt = true;
                    }
                }, this);
              if(canITakeIt){  
                this.takeOrder(order);
                // console.log(this.name+" get a WorkingOrder in " + this.home + "! (WO-> "+order.objective.name+")");
                var index = workingOrders.indexOf(order);
                if (index > -1) {
                    workingOrders.splice(index, 1);
                    // console.log("deletei essa porra" + this.home + "!");
                }
              }
            }, this);
        }
    }

    solvingOrders(order){
        this.operationRequirement.forEach(function(attribute){
            var temp = null;

            temp = this.attributes.getRelevantAttribute(attribute.skill);
            if(typeof temp === "string"){
                temp = this.skills.getRelevantSkill(attribute.skill);
            }
            if(typeof temp !== "string"){
                temp.addXp(temp.level*attribute.effort);
            }
            if(order.objective.maintenanceTime <= order.objective.maintenanceTimeMax){
                order.objective.maintenanceTime += (temp.level+attribute.effort)*10;
            }else{
                this.finishOrder(order)
            }
        }, this);
    }

    takeOrder(order){
        this.working = true;
        this.order = order;
        this.operationRequirement = order.objective.operationRequirement;
    }

    finishOrder(order){
        this.order.moduleControl.isOrderCreated = false;
        this.working = false;
    }
    
    decisions(something){
        switch(something) {
          case "I'm free":
                //that's the shit I do when I'm free
            break;
          case "I'm working":
            // I can't really do any shit when I'm working but if I get tired maybe I can catch a break?
            break;
          case "I'm on my break":
            // don't tell any one about it but I will be jerking off in the toilet
            break;
        }
    }

    update(delta){
        if(!this.didISayHi){
        	this.didISayHi = true;
        }
        if(this.working){
            this.solvingOrders(this.order);
        }

        this.biology();
    }
}