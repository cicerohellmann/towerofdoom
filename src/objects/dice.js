class Dice {
	constructor(diceSides_){
		this.diceSides = diceSides_;
	}

	rollDice(){
		return (Math.floor(Math.random() * this.diceSides) +1);
	}
}