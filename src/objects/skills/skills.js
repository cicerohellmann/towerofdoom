class Skills {
    constructor(){
        this.skillType = new SkillType();
        this.repairing = new BaseLearning(0,400,3);
        this.cleaning = new BaseLearning(0,400,3);
    }

    skillHighest(){
        var largest = new BaseLearning(0,0,0);
        this.skillList.forEach(function(skill){
          if(largest.level < skill.level) 
          largest = skill;
        });
        return largest.baseType;
    }

    getRelevantSkill(attributeType){
        switch(attributeType) {
            case this.skillType.REPAIRING:
                return this.repairing
                break;
            case this.skillType.CLEANING:
                return this.cleaning
                break;
            default:
                return "it's an attribute"
        }
    }
}