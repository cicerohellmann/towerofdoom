class Attributes {
    constructor(strenght_,dexterity_,constitution_,wisdom_,intelligence_,charisma_){
    	this.strenght = strenght_;
		this.dexterity = dexterity_;
		this.constitution = constitution_;
		this.wisdom = wisdom_;
		this.intelligence = intelligence_;
		this.charisma = charisma_;
        this.skillType = new SkillType();
    }

    life(){
    	return this.constitution.level * 5;
    }

    isIntelligent(){
    	if(this.intelligence.level<5){
    		return false;
    	}
    	return true;
    }

    getRelevantAttribute(attributeType){
        switch(attributeType) {
            case this.skillType.STRENGHT:
                return this.strenght
                break;
            case this.skillType.DEXTERITY:
                return this.dexterity
                break;
            case this.skillType.CONSTITUTION:
                return this.constitution
                break;
            case this.skillType.WISDOM:
                return this.wisdom
                break;
            case this.skillType.INTELLIGENCE:
                return this.intelligence
                break;
            case this.skillType.CHARISMA:
                return this.charisma
                break;
            default:
                return "it's a skill"

        }
    }
}