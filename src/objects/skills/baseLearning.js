class BaseLearning {
    constructor(xpCurrent_,xpNext_,level_){
		this.xpCurrent = xpCurrent_;
		this.xpNext = xpNext_;
		this.level = level_;
    }

    addXp(xp){
    	this.xpCurrent = xp + this.xpCurrent;
    	if(this.xpNext <= this.xpCurrent){
    		this.xpNext += this.xpNext;
    		this.level += 1;
    	}
    }
}