class DeficiencyNeeds {
	constructor(){
		this.motivation = 0; //the bigger the motivation, the more the creature wants to do something 
		this.motivationMin = 0;
		this.motivationIncreaseRate = 1;
		this.mindStability = 5; //how much your mind accepts that you are satisfied
	}	
	
	needsAreMeet(){
		//Motivation decreases when needs are met
		this.motivation = (this.motivationMin + Math.floor(Math.random() * mindStability));   
	}

	needsGrow(){
		// Motivation grows when there is need
		this.motivation = this.motivation + (Math.floor(Math.random() * 2/mindStability));
	}

	update(delta){
		this.needsGrow();
	}
}