class StationController {
    constructor(modules_){
        this.workingOrders = [];
        this.moduleControlList = [];
        modules_.forEach(module => {
            this.moduleControlList.push(new ModuleControl(false, (8000), module))
        }, this);
    }

    update(delta){
    	this.moduleControlList.forEach(function(moduleControl){
          if(moduleControl.module.maintenanceTime <= moduleControl.maintenanceTimeExpected && moduleControl.isOrderCreated == false){
            moduleControl.isOrderCreated = true;
            this.createWorkingOrder(moduleControl);
          }
        }, this);
    }

    createWorkingOrder(moduleControl){
    	this.workingOrders.push(new WorkingOrder(moduleControl))
    }
}