
class Planet {
    constructor(name_,size_,position_){
        this.name = name_;
        this.size = size_;
        this.position = position_;
       // this.sunLight = new THREE.PointLight(new THREE.Color(0xffffff), 1.0);
        this.init();
    }
    init(){
        //this.sunLight.position.set(100, 0, 0);
        console.log('Station orbit to :'+this.name);
        this.creationMesh();

       
     

    }
    creationMesh(){
        var group = new THREE.Group();
        const geo = new THREE.SphereGeometry(this.size,  32,  32);
        const geoPlanet = new THREE.SphereGeometry(this.size-0.2,  32,  32);
        geo.phiStart  = 100;
        geo.phiLength  = 100;
        const material = new THREE.MeshPhongMaterial({
            color:0x545E4B,
            opacity: 0.6,
            transparent: true,
        });
        material.flatShading = true;
        const materialPlanet = new THREE.MeshStandardMaterial({
            color: 0x5D7F4D,
            emissive: 0x545E4B,
            metalness: 0.9,
            roughness: 0.75
        });
        material.flatShading = true;
        var  planet = new THREE.Mesh(geoPlanet,materialPlanet);
        var  planetProto = new THREE.Mesh(geo,material);
        group.add( planet );
        group.add( planetProto );

        //const edges = new THREE.EdgesGeometry(geo);
        //var  mesh2 = new THREE.LineSegments(edges,new THREE.LineBasicMaterial({color:0x00ff0000}));
        const atmosphereGeometry = new THREE.SphereGeometry(this.size+0.25,  32,  32);
        const atmosphere = new THREE.Mesh(atmosphereGeometry, this.glowMaterialAtmosphere(2, 3.1, 0x3DD8FF,[this.position[0],this.position[1],this.position[2]]));
        group.add(atmosphere);
        group.position.set(this.position[0],this.position[1],this.position[2]);
        scene.add(group);

    }
    shaderMaterial(){
        var vertexShader = "\
            varying vec3 vNormal;\
            varying vec3 cameraVector;\
            varying vec3 vPosition;\
            varying vec2 vUv;\
            \
            void main() {\
                vNormal = normal;\
                vec4 vPosition4 = modelMatrix * vec4(position, 1.0);\
                vPosition = vPosition4.xyz;\
                cameraVector = cameraPosition - vPosition;\
                vUv = uv;\
                \
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\
            }\
        ";
        
        var fragmentShader = "\
            uniform vec3 pointLightPosition;\
            uniform sampler2D map;\
            uniform sampler2D normalMap;\
            \
            varying vec3 vNormal;\
            varying vec3 vPosition;\
            varying vec3 cameraVector;\
            varying vec2 vUv;\
            \
            mat4 rotationMatrix(vec3 axis, float angle) {\
                axis = normalize(axis);\
                float s = sin(angle);\
                float c = cos(angle);\
                float oc = 1.0 - c;\
                \
                return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,\
                            oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,\
                            oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,\
                            0.0,                                0.0,                                0.0,                                1.0);\
            }\
            \
            vec3 bumpNormal(sampler2D normalMap, vec2 vUv) {\
                vec3 bumpedNormal = normalize(texture2D(normalMap, vUv).xyz * 2.0 - 1.0);\
                \
                vec3 y_axis = vec3(0,1,0);\
                float rot_angle = acos(dot(bumpedNormal,y_axis));\
                vec3 rot_axis = normalize(cross(bumpedNormal,y_axis));\
                return vec3(rotationMatrix(rot_axis, rot_angle) * vec4(vNormal, 1.0));\
            }\
            \
            void main() {\
                float PI = 3.14159265358979323846264;\
                vec3 light = pointLightPosition - vPosition;\
                vec3 cameraDir = normalize(cameraVector);\
                vec3 newNormal = bumpNormal(normalMap, vUv);\
                \
                light = normalize(light);\
                \
                float lightAngle = max(0.0, dot(newNormal, light));\
                float viewAngle = max(0.0, dot(vNormal, cameraDir));\
                float adjustedLightAngle = min(0.6, lightAngle) / 0.6;\
                float adjustedViewAngle = min(0.65, viewAngle) / 0.65;\
                float invertedViewAngle = pow(acos(viewAngle), 3.0) * 0.4;\
                \
                float dProd = 0.0;\
                dProd += 0.5 * lightAngle;\
                dProd += 0.2 * lightAngle * (invertedViewAngle - 0.1);\
                dProd += invertedViewAngle * 0.5 * (max(-0.35, dot(vNormal, light)) + 0.35);\
                dProd *= 0.7 + pow(invertedViewAngle/(PI/2.0), 2.0);\
                \
                dProd *= 0.5;\
                vec4 atmColor = vec4(dProd, dProd, dProd, 1.0);\
                \
                vec4 texelColor = texture2D(map, vUv) * min(asin(lightAngle), 1.0);\
                gl_FragColor = texelColor + min(atmColor, 0.8);\
            }\
        ";
	
        var uniforms = {
             "pointLightPosition": {"type": "v3", "value":[10,10,10]},
           // "map": {"type": "t", "value": this.createMap()},
       //     "normalMap": {"type": "t", "value": SS.util.heightToNormalMap(map)}
        };

        return new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            transparent: true
        });
    }
    glowMaterialAtmosphere(intensity, fade, color,pos){

        let glowMaterial = new THREE.ShaderMaterial({
            side: THREE.DoubleSide,
          transparent: true,
          uniforms: { 
            'c': {
              type: 'f',
              value: intensity
            },
            'p': { 
              type: 'f',
              value: fade
            },
            glowColor: { 
              type: 'c',
              value: new THREE.Color(color)
            },
            viewVector: {
              type: 'v3',
              value: pos
            }
          },
          vertexShader: `
            uniform vec3 viewVector;
            uniform float c;
            uniform float p;
            varying float intensity;
            void main() {
              vec3 vNormal = normalize( normalMatrix * normal );
              vec3 vNormel = normalize( normalMatrix * viewVector );
              intensity = pow( c - dot(vNormal, vNormel), p );
              gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }`
          ,
          fragmentShader: `
            uniform vec3 glowColor;
            varying float intensity;
            void main() 
            {
              vec3 glow = glowColor * intensity;
              gl_FragColor = vec4( glow, 1.0 );
            }`
          ,
          side: THREE.BackSide,
          blending: THREE.AdditiveBlending,
          transparent: true
        });
        
        return glowMaterial;
      
    }
}