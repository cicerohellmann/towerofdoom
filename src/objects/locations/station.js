class Station {
    constructor(id_,size_,pos_,maxPopulation_,currentPopulation_,currentMoral_,storageLimit_, population_,modules_){
        this.id = id_;
        this.pos = pos_;
        this.size = size_;
        this.maxPopulation = maxPopulation_; 
        this.minPopulation = 0; 
        this.currentPopulation = currentPopulation_;
        this.integrity = 2;
        this.alive = true;
        this.currentMoral = currentMoral_;
        this.storageLimit = storageLimit_;
        this.establishments = new Establishment();
        this.stationMeshOffside =  this.creationMeshOffside();
        this.stationMeshInside =  this.creationMeshInside();
        this.limitRandSize = 5;
        this.population = population_;
        this.modules = modules_;
        this.stationController = new StationController(modules_)
        this.workingOrders = [];
        this.randMeshX  = 0;
        this.randMeshY  = 0;
        this.explode = new Explosion();
    }
    
    creationMeshOffside(){
        this.randMeshX  = THREE.Math.randFloatSpread( 10 );
        this.randMeshY  = THREE.Math.randFloatSpread( 10 );
        const geo = new THREE.SphereGeometry(3,  this.randMeshX+1,  this.randMeshY+1);
        geo.phiStart  = 100;
        geo.phiLength  = 100;
        const material = new THREE.MeshStandardMaterial({
            color: 0x3F3F3F,
            emissive: 0x3F3F3F,
            // specular: 0x9E9E9E,
            metalness: 2,
            roughness: 3,
        });
        material.flatShading = false;
        var  mesh = new THREE.Mesh(geo,material);
       
        const edges = new THREE.EdgesGeometry(geo);
        var  mesh2 = new THREE.LineSegments(edges,new THREE.LineBasicMaterial({color:0x494949}));

    
        var group = new THREE.Group();
        group.name = "stationMeshOffside-"+this.id;
        group.add( mesh );
        group.add( mesh2 );

        return group;
    }
    creationMeshInside(){  
        var floorTexture = new THREE.TextureLoader().load('src/textures/metal_surface.jpg');

        floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
        floorTexture.repeat.set( 20, 150 );
        var floorMaterial = new THREE.MeshBasicMaterial( { map: floorTexture, side: THREE.DoubleSide } );
        var  floorGeometry = new THREE.PlaneGeometry(7, 7, 1, 1);
        var  floor = new THREE.Mesh(floorGeometry, floorMaterial);
        floor.position.y = -0.5;
        floor.rotation.x = Math.PI / 2;

        const geo = new THREE.SphereGeometry(6, this.randMeshX+1,  this.randMeshY+1);
        const mat = new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, opacity: 0.5, transparent: true } );


        var  mesh = new THREE.Mesh(geo,mat);
        mesh.material.side = THREE.BackSide;
        const edges = new THREE.EdgesGeometry(geo);
        var  mesh2 = new THREE.LineSegments(edges,new THREE.LineBasicMaterial({color:0x969696}));

        var group = new THREE.Group();
        group.name = "stationMeshInside-"+this.id;
        group.add( floor );
        group.add( mesh );
        group.add( mesh2 );

        return group;
    }
    creationMeshEplode(){
        this.explode.init(this.stationMeshOffside.position.x,this.stationMeshOffside.position.y,this.stationMeshOffside.position.z);
        scene.remove(this.stationMeshOffside);
        scene.remove(this.stationMeshInside);
    }

    isAlive(){
        if(this.integrity<=0 && this.alive){
            console.log(this.id+" BOOM");
            this.alive = false;
            this.creationMeshEplode();
        }else if(this.alive){
            //this.integrity-= 1*delta;
        }
        
    }
    
    update(delta){
        this.isAlive();
        if(!this.alive){
            this.explode.update(); 
        }

        this.stationController.update(delta)
        this.population.forEach(person => {
            person.update(delta);
            // person.watchOrders(this.workingOrders);
            // console.log(this.stationController.workingOrders);
            person.watchOrders(this.stationController.workingOrders);
        }, this);  

        
        this.modules.forEach(function(module){
            module.update(delta);
          if(module.isMaintenanceTime == true && module.isOrderCreated == false){
            this.createWorkingOrder(module);
            module.isOrderCreated = true;
          }
        }, this);
    }
}