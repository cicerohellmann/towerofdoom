
class StarField {
    constructor(numberOfStars_ , width_ , height_, position_){
        this.numberOfStars = numberOfStars_;
        this.width = width_;
        this.height = height_;
        this.position = position_;
        this.init();
    }
    init(){
        var skyBox = new THREE.BoxGeometry(120, 120, 120);
        var skyBoxMaterial = new THREE.MeshBasicMaterial({
            map: this.createStarField(this.numberOfStars, this.width, this.height),
            side: THREE.BackSide
        });
        var sky = new THREE.Mesh(skyBox, skyBoxMaterial);
        sky.position.set(this.position[0], this.position[1], this.position[2]);
        scene.add(sky);
    }

    createStarField(numberOfStars, width, height) {
        var canvas = document.createElement('CANVAS');
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext('2d');
        for (var i = 0; i < numberOfStars; ++i) {
            var radius = Math.random() * 2;
            var x = Math.floor(Math.random() * width);
            var y = Math.floor(Math.random() * height);
            ctx.beginPath();
            ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = 'white';
            ctx.fill();
        }
    
        var texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        return texture;
    }
    render(){
        
    }
    update(delta){
        /*
        switch (this.type) {
            case "simple":
                    this.mesh.rotation.y += 0.001;
                    this.starField.rotation.x += 0.001;
                break;
            case "void":
                    this.mesh.rotation.y += 0.1 * delta;

                break;     
            default:
                break;
        }
        */
    }
    /*
    constructor(colorBase,colorMiddle,sizeStar,amount,randSpread,type_){
        this.mesh = null;
        this.starsGeometry = new THREE.Geometry();
        this.colorBase = colorBase;
        this.colorMiddle = colorMiddle;
        this.amount = amount;
        this.randSpread = randSpread;
        this.texture = this.generateSpriteStar(colorBase,colorMiddle,sizeStar);
        this.type = type_;
        this.clock = null;
        // starsVOidUpdade
        this.rotateCount = 0;
        this.rotateCountMax = 10000;
        this.speedStarX = 0.1;
        this.speedStarY = 0.1;
        this.speedStarZ = 0.1;
        this.changeStarRun = false;
        this.t = Math.random() * 100;
        this.speed = 0.01 + Math.random() / 200;
        this.factor = 20 + Math.random() * 100;
        this.xFactor = -50 + Math.random() * 100;
        this.yFactor = -50 + Math.random() * 100;
        this.zFactor = -30 + Math.random() * 60;
        //----------------

    }
    createStarField(position){
        switch (this.type) {
            case "simple":
                    this.starsMaterial = new THREE.PointsMaterial({
                        size: 5,
                        transparent: true,
                        blending: THREE.AdditiveBlending,
                        map: this.texture 
                    });
                    this.starsMaterial.morphTargets = true;
                    this.starsMaterial.sizeAttenuation = true;
                    this.starsMaterial.size = 0.3;
             
                     for ( var i = 0; i < this.amount; i ++ ) {
                         var star = new THREE.Vector3();
                         star.x = THREE.Math.randFloatSpread( this.randSpread );
                         star.y = THREE.Math.randFloatSpread( this.randSpread );
                         star.z = THREE.Math.randFloatSpread( this.randSpread );
                         star.set(position[0], position[1], position[2]);
                         this.starsGeometry.vertices.push( star );
                     }
                     this.mesh = new THREE.Points( this.starsGeometry, this.starsMaterial );
                                              star.set(position[0], position[1], position[2]);

                     scene.add( this.mesh );
                break;
            case "void":
                    this.starsMaterial = new THREE.PointsMaterial({
                        size: 1,
                        transparent: true,
                        blending: THREE.AdditiveBlending,
                        map: this.texture 
                    });
                    this.starsMaterial.morphTargets = true;
                    this.starsMaterial.sizeAttenuation = true;
                    this.starsMaterial.size = 0.1;
                    this.starsGeometry.dynamic = true;
                    for ( var i = 0; i < this.amount; i ++ ) {
                        
                        var star = new THREE.Vector3();
                        star.x = THREE.Math.randFloatSpread( this.randSpread );
                        star.y = (THREE.Math.randFloatSpread( this.randSpread ));
                        star.z = THREE.Math.randFloatSpread( this.randSpread );
                        this.starsGeometry.vertices.push( star );

                    }
                    this.mesh = new THREE.Points( this.starsGeometry, this.starsMaterial );

                    this.mesh.rotation.x = 0.5;
                    this.mesh.updateMatrixWorld();
                    this.mesh.dynamic = true;
                    this.mesh.acceleration = new THREE.Vector3();
                    scene.add( this.mesh );

                break; 
            default:
                break;
        }

    }
    render(){
        
    }

  

    update(delta){
        
        switch (this.type) {
            case "simple":
                    this.mesh.rotation.y += 0.001;
                    this.starField.rotation.x += 0.001;
                break;
            case "void":
                    
                    this.mesh.rotation.y += 0.1 * delta;

                break;     
            default:
                break;
        }

    }
    generateSpriteStar(colorBase,colorMiddle,sizeStar){
        var canvas = document.createElement('canvas');
        canvas.width = sizeStar;
        canvas.height = sizeStar;
        var context = canvas.getContext('2d');
        var gradient = context.createRadialGradient(canvas.width /2, canvas.height /2, 0, canvas.width /2, canvas.height / 2, canvas.width / 2);
        gradient.addColorStop(0, colorBase);
        gradient.addColorStop(.5, colorMiddle);
        gradient.addColorStop(.9, 'rgba(255,255,255,0)');
        gradient.addColorStop(1, 'rgba(0,0,0,0)');
        context.fillStyle = gradient;
        context.fillRect(0, 0, canvas.width, canvas.height);
        var texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        return texture;
    }*/
}
