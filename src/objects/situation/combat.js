class Combat {
	constructor(team1_,team2_){
		this.team1 = team1_;
		this.team2 = team2_;
		this.combat = [];
		this.combatFinished = false;
		this.winningTeam = null;
		this.team1.forEach(unit => this.combat.push(unit));
		this.team2.forEach(unit => this.combat.push(unit));

		this.rollInitiative();
		this.initiativeSort();
		this.combatRound();
	}

	rollInitiative(){
		this.combat.forEach(unit => {
			unit.initiativeCurrent = (new Dice(20).rollDice() + unit.initiative + unit.attributes.dexterity.level)
		}, this);
	}

    winCheck(){
    	var team1Members = 0;
    	var team2Members = 0;
    	this.combat.forEach(unit => {
    		if(this.team1[0].crewID == unit.crewID){
				team1Members = team1Members+1;
    		}else{
    			team2Members = team2Members+1;
    		}
    	}, this);
    	    if(team1Members <= 0){
				this.combatFinished = true;
				this.winningTeam = this.team1[0].crewID;
    		}else if(team2Members <= 0){
				this.combatFinished = true;
				this.winningTeam = this.team2[0].crewID;
    		}
    }

    combatRound(){
        this.combat.forEach(attacker => {
            this.combat.forEach(defender => {
                if(attacker.crewID != defender.crewID){
                    if(attacker.hit(attacker.mainHand) > defender.dc){
                    	var damage = (new Dice(3).rollDice()+attacker.attributes.strenght.level/2);
                        defender.life = defender.life - damage;
                        if(defender.life <= 0){
                            defender.disabled = true;
                            var index = this.combat.indexOf(defender);
                            if (index > -1) {
                              this.combat.splice(index, 1);
                            }
                            console.log(attacker.name + " acertou um golpe de " + damage + "!");
                            console.log("e " + defender.name + " foi desabilitado");   
                        }else{
                            console.log(attacker.name + " acertou um golpe de " + damage + "!");
                            console.log("e " + defender.name + " ficou com " + defender.life + " de vida");           
                        }
                    }else{
                        console.log(attacker.name + " errou o golpe!");
                    }
                }
            }, this);
        }, this);
    }

	initiativeSort(){
		this.combat = this.combat.sort((a, b) => {
			return (a.initiativeCurrent - b.initiativeCurrent)
		}, this);
	}

	update(){
		this.winCheck();
		if(this.combatFinished == false){
			this.combatRound();
		}
	}
}