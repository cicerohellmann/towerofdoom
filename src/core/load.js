class Load{
    constructor(){

        this.loadList =  new LoadLists();
        //this.starField = new StarField("white","rgba(255,255,255,10)",32,1000,100,'void');
        this.starField = new StarField(600, 2048, 2048,[0,0,0]);
        this.ambientLight = new THREE.AmbientLight(0xFFFFFF,0.1,0.1);
        this.directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
        this.group = new THREE.Group();
    }

    init(){
        this.starField.createStarField([0,0,0]);

        var geometry = new THREE.BoxGeometry( 2, 2, 2 );

        for(var i=0;i<4;i++){
            const mat = new THREE.MeshLambertMaterial({ color: Math.random() * 0xffffff}); 
            var cube = new THREE.Mesh( geometry, mat );
            cube.position.set(((-4)+(i*3)), 0, 0);
            this.group.add( cube );

        }
        scene.add( this.ambientLight );
        scene.add( this.directionalLight );
        scene.add( this.group); 
        this.getLoadList();
        
    }

    render(){
       // this.starField.render();
    }

    update(delta){
        this.group.rotation.x += 0.01;
        this.starField.update(delta);
    }
    
    getLoadList(){
        this.loadList.init();
        this.loadWait();
    }

    loadWait(){
        var i = 0;
        var loadTime = 1;
        var loop = setInterval(function(){ 
          console.log("load game IN "+i+" /"+loadTime); 
          if(i==loadTime){
            clearInterval(loop);
            changeForGame();
          }
          i++;
        }, 1000);
    }

}
