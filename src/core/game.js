class Game{
    constructor(){
        this.loadList = null;
        this.sceneControl = null; 
        // Lights
        this.ambientLight = null;
        this.directionalLight = null;
        //------------ 
        this.controls = null;
    }

    init(loadList_){
        this.ambientLight = new THREE.AmbientLight(0xFFFFFF,0.1,0.1);
        //this.directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
        this.controls = new THREE.OrbitControls( camera, renderer.domElement );
        this.loadList = loadList_;
        camera.position.set( 0, 0, 20 );
        this.sceneControl = new SceneControl(scene,this.loadList.stationList,this.controls);
       // scene.add( this.ambientLight );
       // scene.add( this.directionalLight );
        // limit controlss


    }

    render(){
        


    }
    update(delta){
        this.loadList.stationList.forEach(function(station){
            station.update(delta);    
        }, this);
        this.sceneControl.update(delta);
    }

}
