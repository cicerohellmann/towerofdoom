class Menu{
    constructor(){
        this.loadList = null;
        this.ambientLight = new THREE.AmbientLight(0xFFFFFF,0.1,0.1);
        this.directionalLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
        this.group = new THREE.Group();
        this.starField = new StarField("white","rgba(255,255,255,10)",32,1000,400,'simple');
        this.cavnasMenu  = new CanvasMenu();
    }

    init(loadList_){
        this.cavnasMenu.init();
      //  this.cavnasMenu.delete();
        
        this.loadList = loadList_;
        scene.add( this.ambientLight );
        scene.add( this.directionalLight );
        scene.add(this.starField.createStarField());

    }

    render(){
        this.starField.render();
    }

    update(delta){

    }
   

}
