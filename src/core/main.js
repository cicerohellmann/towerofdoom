var clock = new THREE.Clock();
var scene = new THREE.Scene();
var sceneOverlay = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );


var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: false } );
renderer.setClearColor(0x000000, 0);
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
//renderer.domElement.style.position = 'absolute';
//renderer.domElement.style.top = 0;
//renderer.domElement.style.zIndex = '0';

/*var rendererOverlay = new THREE.WebGLRenderer({ antialias: true, alpha: true } );
rendererOverlay.setClearColor(0xffffff, 0);
rendererOverlay.domElement.style.position = 'absolute';
rendererOverlay.domElement.style.top = 0;
rendererOverlay.domElement.style.zIndex = '1';
*/

var load = new Load();
var menu = null;
var game = null;
var loadRun = true;
var menuRun = false;
var gameRun = false;

var changeForMenu = function(){
    //transition = new Transition(scene,scene);
    clearScene();
    loadRun = false;
    menuOver = true;
    gameRun = false;
    console.log("INIT Menu");
    menu = new Menu();
    menu.init(load.loadList);
}

var changeForGame = function(){
    //transition = new Transition(scene,scene);
    clearScene();
    loadRun = false;
    menuOver = false;
    gameRun = true;
    console.log("INIT Game");
    game = new Game();
    game.init(load.loadList);
}


var clearSceneOverlay = function(){
    while(sceneOverlay.children.length > 0){ 
        sceneOverlay.remove(sceneOverlay.children[0]); 
    }
}
var clearScene = function(){
    while(scene.children.length > 0){ 
        scene.remove(scene.children[0]); 
    }
}
function removeEntityByName(objectBame) {
    var selectedObject = scene.getObjectByName(objectBame);
    scene.remove( selectedObject );
    render();
}

var init = function(){
    window.addEventListener('resize',function()
    {
        var width = window.innerWidth;
        var height = window.innerHeight;
        renderer.setSize(width,height);
        camera.aspect = width / height;
        camera.updateProjectionMatrix();
    });
    renderer.gammaOutput = true;
    renderer.gammaFactor = 2.2; // approximate sRGB
    document.body.appendChild( renderer.domElement );
  //  rendererOverlay.gammaOutput = true;
  //  rendererOverlay.gammaFactor = 2.2; // approximate sRGB
  //  document.body.appendChild( rendererOverlay.domElement );

    camera.updateMatrixWorld();
    camera.position.set( 0, 0, 10 );
    camera.lookAt( 0, 0, 0 );

    load.init();
};

var update = function(delta){
    if(loadRun){
        load.update(delta);
    }else
    if(menuRun){
        menu.update(delta);
    }else
    if(gameRun){
        game.update(delta);
    }
}

var render = function(){
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.render( scene, camera );
    //rendererOverlay.setSize( window.innerWidth, window.innerHeight );
    //.render( sceneOverlay, camera );

    if(loadRun){
        load.render();
    }else
    if(menuRun){
        menu.render();
    }else
    if(gameRun){
        game.render();
    }

}

var GameLoop = function(ts){
    delta = clock.getDelta();
    requestAnimationFrame( GameLoop );
    update(delta);
    render();
};

init();
GameLoop();