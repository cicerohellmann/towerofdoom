class LoadLists {
   
    #jsonSaveGame = "save/saveGame.json";

    constructor(){
        this.stationList = [];

    }

    init(){
        this.jsonLoadSaveGame();
    }

    jsonLoadSaveGame(){
        var auxList = [];
        $.getJSON(this.#jsonSaveGame, function(json){
            json.saveList.forEach(function(saveList){
                var chapArray = [];
                saveList.population.forEach(function(chap){
                    var skills = [];
                    chap.skills.forEach(function(skill){
                        skills.push(new Attributes(
                            new BaseLearning(skill.baseLearning[0],skill.baseLearning[1],skill.baseLearning[2],skill.skillName),//strenght_
                        ))
                    })
                    chapArray.push(
                        new Chap(
                            chap.age,
                            chap.alive,
                            new Attributes(
                                    new BaseLearning(chap.attributes[0].strenght[0],chap.attributes[0].strenght[1],chap.attributes[0].strenght[2]),//strenght_
                                    new BaseLearning(chap.attributes[0].dexterity[0],chap.attributes[0].dexterity[1],chap.attributes[0].dexterity[2]),//dexterity_
                                    new BaseLearning(chap.attributes[0].constitution[0],chap.attributes[0].constitution[1],chap.attributes[0].constitution[2]),//constitution_
                                    new BaseLearning(chap.attributes[0].wisdom[0],chap.attributes[0].wisdom[1],chap.attributes[0].wisdom[2]),//wisdom_
                                    new BaseLearning(chap.attributes[0].intelligence[0],chap.attributes[0].intelligence[1],chap.attributes[0].intelligence[2]),//intelligence_
                                    new BaseLearning(chap.attributes[0].charisma[0],chap.attributes[0].charisma[1],chap.attributes[0].charisma[2]) //charisma_
                            ),
                            [new Weapon([new Dice(8), new Dice(6)],new SkillType().STRENGHT,new SlotType().MAINHAND,"The murderer", 200, 2.5,new EquipmentType().WEAPON, "Muerdering blast hole"),
                            new Armor(2,new SlotType().HEAD,"The Hat", 1, 0.5,new EquipmentType().ARMOR, "Extreme protection agains UV rays")],
                            chap.gender,
                            chap.livingConditions,
                            chap.name,
                            chap.perks,
                            skills,
                            chap.species,
                            chap.home,
                            chap.where
                        )
                    );
                },this)
                auxList.push(new Station(
                    saveList.name,
                    saveList.size,
                    saveList.position,
                    saveList.maxPopulation,
                    saveList.currentPopulation,
                    saveList.currentMoral,
                    saveList.storageLimit,
                    chapArray,
                    [new Module()]
                ));
            },this);
        });
        //console.log(auxList);
        this.stationList = auxList;
    }
}

