class SceneControl{
    constructor(scene_,stationsList_,controls_){
        this.scene = scene_;
        this.controls = controls_;
        this.currentScene = "";
        this.stationsList = stationsList_;
        this.controlScenes("tableTop","chaca-666")
    }

    controlScenes(nameScene,info){
        switch (nameScene) {
            case "stationScene":
                this.stationScene(info);
                break;
            case "orbitStation":
                this.orbitStationScene(info);
                break;
            case "universe":
                this.universeScene(info);
                break;
            case "universeMap":
                this.universeMapScene();
                break;
            case "tableTop":
                this.tabletopScene();
                break;
            default:
                break;
        }
    }

    tabletopScene(){
        this.clearScene();
        this.currentScene = new SpaceshipScene();
        // this.currentScene.update()
    }

    stationScene(IDStation){
        this.clearScene();
        this.currentScene  = new StationScene(this.stationsList[IDStation]);
        this.currentScene.draw()
    }

    orbitStationScene(IDStation){
        this.clearScene();
        this.currentScene  = new OrbitStationScene(this.stationsList[IDStation]);
        this.currentScene.draw();
    }
     
    universeScene(lookFor){
        this.clearScene();
        this.currentScene  = new UniverseScene(this.stationsList,lookFor,this.controls);
        this.currentScene.draw();
    }

    universeMapScene(){
        this.clearScene();
        this.currentScene  = new UniverseMapScene(this.stationsList);
        
        this.currentScene.draw();
    }

    update(delta){
        this.currentScene.update(delta);
    }
    
    clearScene(){
        while(this.scene.children.length > 0){ 
            this.scene.remove(scene.children[0]); 
        }
    }

}