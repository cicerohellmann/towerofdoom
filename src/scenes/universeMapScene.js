class UniverseMapScene{
    constructor(station_){
        this.station = station_;
        //this.draw = this.draw();
    }

    draw(){
        this.station.forEach(station => {
            station.stationMeshOffside.position.x = station.pos[0].posX;
            station.stationMeshOffside.position.y = station.pos[0].posY;
            station.stationMeshOffside.position.z = station.pos[0].posZ;
            scene.add(station.stationMeshOffside);
        });

        scene.add(this.drawStars());
    }
    drawStars(){
        var starField = new StarField("white","rgba(255,255,255,10)",32,10000,1650,"simple");
        return starField.createStarField();
    }
    update(delta){
       // this.station.stationMeshOffside.rotation.x += 0.001;
       // this.station.stationMeshOffside.rotation.y += 0.001;
       // this.station.stationMeshOffside.rotation.z += 0.000;
    }
}