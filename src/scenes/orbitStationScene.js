class OrbitStationScene{
    constructor(station_){
        this.station = station_;
    }

    draw(){
        console.log("Station :"+this.station.id);
        scene.add(this.station.stationMeshOffside);
        scene.add(this.drawStars());
    }
    drawStars(){
        var starField = new StarField("white","rgba(255,255,255,10)",32,10000,1550,"simple");
        return starField.createStarField();
    }
    update(delta){
       // this.station.stationMeshOffside.rotation.x += 0.001;
       // this.station.stationMeshOffside.rotation.y += 0.001;
       // this.station.stationMeshOffside.rotation.z += 0.000;
    }
}