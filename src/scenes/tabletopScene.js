class SpaceshipScene{
    constructor(){
    // constructor(lookFor,controls){
        // this.lookFor = lookFor;
        // this.controls = controls;
        this.controlsMinDistance = 10;
        this.controlsMaxDistance = 45;
        this.cameraDistance = 15;
        this.directionalLight =  new THREE.DirectionalLight( 0xE1E510, 0, 0 );
        this.control = new function () {
            this.hemiLight = true;
            this.dirLight = true;
            this.rotSpeed = 0.01;
        };
        this.navigation = [0][0];
        this.posX = 0;
        this.posZ = 0;
        this.pointer = 0;


        var s = "x";
        var o = " ";
        var q = "/";
        var z = "\\"; 
        var a = "|";
        var w = "_";
        var x = "_";
        var d = "z"; 

        this.items = [
          [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, o, o, o, o, q, w, w, z, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, q, w, w, q, s, s, s, s, z, w, w, w, w, w, w, w, w, w, w, w],
          [q, s, s, s, s, s, s, s, s, a, s, s, s, s, s, s, s, s, s, s, a],
          [a, s, s, s, s, s, s, s, s, d, s, s, s, s, s, s, s, s, s, s, a],
          [a, s, s, s, s, s, s, s, s, d, s, s, s, s, s, s, s, s, s, s, a],
          [z, s, s, s, s, s, s, s, s, a, s, s, s, s, s, s, s, s, s, s, a],
          [o, z, x, x, z, s, s, s, s, q, x, x, x, x, x, x, x, x, x, x, x],
          [o, o, o, o, o, z, x, x, q, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
          [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o]
          ];

        this.limitX = this.items.length;
        this.limitZ = this.items[0].length;
        
        this.posX = this.checkAvailableSpot()[0]
        this.posZ = this.checkAvailableSpot()[1]
        this.addPointer();

        this.drawMatrix();

        window.addEventListener('keydown', (e) => {
            var keyCode = e.key;
            if (keyCode == "w") {
                if(this.posZ > 0 ){
                    if(this.items[this.posX][this.posZ - 1] != " "){
                        this.posZ = this.posZ - 1;
                        console.log(this.posZ);
                    }
                }
            } else if (keyCode == "s") {
                if(this.posZ < this.limitZ-1){
                    if(this.items[this.posX][this.posZ + 1] != " "){
                        this.posZ = this.posZ + 1;
                        console.log(this.posZ);
                    }
                }
            } else if (keyCode == "a") {
                if(this.posX > 0){
                    if(this.items[this.posX - 1][this.posZ] != " "){
                        this.posX = this.posX - 1;
                        console.log(this.posX);
                    }
                }
            } else if (keyCode == "d") {
                if(this.posX < this.limitX-1){
                    if(this.items[this.posX + 1][this.posZ] != " "){
                        this.posX = this.posX + 1;
                        console.log(this.posX);
                    }
                }
            } else if (keyCode == " ") {
                // console.log("fart");
            }
        }, this);
    }

    addPointer(){
        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        const edges = new THREE.EdgesGeometry(geometry);
        this.pointer = new THREE.LineSegments(edges,new THREE.LineBasicMaterial({color:0x00FF7F}));
        this.pointer.position.x = this.posX;
        this.pointer.position.y = 1;
        this.pointer.position.z = this.posZ;

        this.pointer.updateMatrix();
        scene.add( this.pointer );    
    }
    drawMatrix(){
        for (var i = 0; i < this.items.length; i++) {
            // get the size of the inner array
            var innerArrayLength = this.items[i].length;
            // loop the inner array
            for (var j = 0; j < innerArrayLength; j++) {

                var geometry = new THREE.BoxGeometry( 1, 1, 1 );
                const edges = new THREE.EdgesGeometry(geometry);
                var  cube = new THREE.LineSegments(edges,new THREE.LineBasicMaterial({color:0x494949}));
                cube.position.x = i;
                cube.position.z = j;
                if(this.items[i][j] != " "){
                    scene.add( cube );
                }
                // console.log('[' + i + ',' + j + '] = ' + this.items[i][j]);
            }
        }
    }

    checkAvailableSpot(){
        for (var i = 0; i < this.items.length; i++) {
            var innerArrayLength = this.items[i].length;
            for (var j = 0; j < innerArrayLength; j++) {
                if(this.items[i][j] != " "){
                    return [i,j];
                }
            }

        }

            console.log(this.items.length);
    }
    update(){
        this.pointer.position.x = this.posX;
        this.pointer.position.z = this.posZ;
    }


}