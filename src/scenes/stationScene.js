class StationScene{
    constructor(station_){
        this.station = station_;
        this.draw = this.draw();
    }

    draw(){
        scene.add(this.station.stationMeshOffside);
        scene.add(this.drawStars());

        /*
        var loader = new THREE.OBJLoader();


        loader.load(
            // resource URL
            'src/models/obj/insideStation/untitled.obj',
            // called when resource is loaded
            function ( object ) {

                //return object ;
                scene.add(object);
            },
            // called when loading is in progresses
            function ( xhr ) {

                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

            },
            // called when loading has errors
            function ( error ) {

                console.log( 'An error happened' );

            }
        );
            */
    }
    drawStars(){
        var starField = new StarField("white","rgba(255,255,255,10)",32,10000,1550,"simple");
        return starField.createStarField();
    }
    update(delta){

    }
    
}