class UniverseScene{
    constructor(station_,lookFor_,controls_){
        this.station  = station_;
        this.lookFor  = lookFor_;
        this.controls = controls_;
        this.controlsMinDistance = 10;
        this.controlsMaxDistance = 45;
        this.cameraDistance = 15;
        this.directionalLight =  new THREE.DirectionalLight( 0xE1E510, 0, 0 );
      //  this.starField = new StarField(1100, 5048, 5048,[1600,1100,1600]);
        this.plnaet = new Planet("cavalinha",45,[1530,1100,1600]);
        this.control = new function () {
            this.hemiLight = true;
            this.dirLight = true;
            this.rotSpeed = 0.01;
        };
    }

    draw(){
        //this.addHemisphereLight();
        this.addDirectionalLight();

        this.station.forEach(station => {
            var posX = station.pos[0].posX;
            var posY = station.pos[0].posY
            var posZ = station.pos[0].posZ;
            station.stationMeshOffside.position.x = posX;
            station.stationMeshOffside.position.y = posY;
            station.stationMeshOffside.position.z = posZ;
            if(this.lookFor == station.id){
                console.log("lookFor: "+this.lookFor);
                camera.position.set( posX,posY, posZ  + this.cameraDistance);
                this.controls.target.set(posX, posY, posZ);
                this.directionalLight.position.set( posX-90, posY, posZ );
                this.directionalLight.updateMatrixWorld();
                this.controls.update();
                this.controls.minDistance = 10;
                this.controls.maxDistance = 45;
                camera.updateMatrixWorld();
                //this.starField.createStarField([posX, posY, posZ]);
            }
            scene.add(station.stationMeshOffside);
        });

    }


    update(delta){
        var x = this.directionalLight.position.x;
        var z = this.directionalLight.position.z;
        this.directionalLight.position.x = x * Math.cos(this.control.rotSpeed) + z * Math.sin(this.control.rotSpeed);
        this.directionalLight.position.z = z * Math.cos(this.control.rotSpeed) - x * Math.sin(this.control.rotSpeed);
    }
    addHemisphereLight() {
        var hemiLight = new THREE.HemisphereLight(0xff4444, 0x44ff44, 0.6);
        hemiLight.position.copy(new THREE.Vector3(0, 500, 0));
        hemiLight.name = 'hemiLight';
        scene.add(hemiLight);
    }
    addDirectionalLight() {
        this.directionalLight = new THREE.DirectionalLight();
        this.directionalLight.position.copy(new THREE.Vector3(70, 40, 50));
        this.directionalLight.shadow.mapSize.width = 512; 
        this.directionalLight.shadow.mapSize.height = 512;
        this.directionalLight.shadow.camera.near = 25;    
        this.directionalLight.shadow.camera.far = 200;    
        this.directionalLight.castShadow = true;
        this.directionalLight.shadow.camera.left = -50;
        this.directionalLight.shadow.camera.right = -50;
        this.directionalLight.shadow.camera.top = -50;
        this.directionalLight.shadow.camera.bottom = -50;
        scene.add(this.directionalLight);
    }

}